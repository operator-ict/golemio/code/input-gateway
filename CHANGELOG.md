# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [2.9.1] - 2025-01-30

### Added

-   PRE energetics input router ([p0262#52](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/52))

## [2.9.0] - 2025-01-20

### Changed

-   Change TS build target from ES2015 to ES2021 ([core#121](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/121))

## [2.8.0] - 2024-12-17

### Changed

-   Update Node.js to v20.18.0
-   Disable Sentry Express middleware ([p0255#90](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/90))

### Fixed

-   Memory leaks related to Sentry Express middleware ([p0255#90](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/90))

## [2.7.0] - 2024-11-21

### Changed

-   Azure Blob Storage managed identity env vars update ([infra#304](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/issues/304))

## [2.6.5] - 2024-11-05

-   No changelog

## [2.6.4] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [2.6.3] - 2024-07-17

-   No changelog

## [2.6.2] - 2024-07-04

### Changed

-   Utilize CsvParserMiddleware to parse csv push data ([ig#82](https://gitlab.com/operator-ict/golemio/code/input-gateway/-/issues/82))

### Removed

-   `body-parser-csv` dependency

## [2.6.0] - 2024-06-24

-   No changelog

## [2.5.13] - 2024-05-22

### Added

-   Adding parkings isphk router ([p0255#30](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/30))
-   Adding energetics and new csv parsing rule ptas router ([prevzeti-ed#4](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/4)

## [2.5.12] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [2.5.11] - 2024-05-09

### Added

-   Adding new router ([parkings#285](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/merge_requests/285))

## [2.5.10] - 2024-04-29

-   No changelog

## [2.5.9] - 2024-04-24

### Fixed

-   Invalid incoming xml returns Error 400 correctly ([core#97](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/97))

## [2.5.8] - 2024-04-15

### Added

-   Expose static openapi docs files

## [2.5.7] - 2024-03-06

-   No changelog

## [2.5.6] - 2024-01-24

### Added

-   Add parking sources input api ([p0255#21](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/21))

## [2.5.5] - 2024-01-22

-   No changelog

## [2.5.4] - 2024-01-17

### Changed

-   Improve README.md
-   Update Swagger UI, improve OpenAPI docs

### Removed

-   Apiary docs (permanently moved to OpenAPI) ([general#498](https://gitlab.com/operator-ict/golemio/code/general/-/issues/498))
-   Dredd tests

## [2.5.3] - 2023-12-20

### Fixed

-   Improve request aborted handling ([core#86](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/86))

## [2.5.2] - 2023-12-06

-   No changelog

## [2.5.1] - 2023-11-22

-   No changelog

## [2.5.0] - 2023-10-09

### Changed

-   Update apiary/openapi docs, change swagger-ui config

## [2.4.9] - 2023-09-27

-   No changelog

## [2.4.8] - 2023-09-18

### Fixed

-   Fix responseTime metric in RequestLogger ([core#73](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/73))

## [2.4.7] - 2023-09-04

-   No changelog

## [2.4.6] - 2023-08-23

### Changed

-   Generate OAS file via Golemio CLI ([core#46](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/46))

## [2.4.5] - 2023-08-07

-   No changelog

## [2.4.4] - 2023-07-31

### Changed

-   Node 18.17.0

## [2.4.3] - 2023-07-24

-   No changelog

## [2.4.2] - 2023-07-17

### Fixed

-   Opentelemetry initialization

## [2.4.1] - 2023-07-10

-   No changelog

## [2.4.0] - 2023-06-26

-   No changelog

## [2.3.10] - 2023-06-21

### Added

-   Input gateway API docs ([input-gateway#80](https://gitlab.com/operator-ict/golemio/code/input-gateway/-/issues/80))

## [2.3.9] - 2023-06-19

### Fixed

-   Body parser syntax error handle ([input-gateway#81](https://gitlab.com/operator-ict/golemio/code/input-gateway/-/issues/81))

## [2.3.8] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [2.3.7] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [2.3.6] - 2023-05-29

-   No changelog

## [2.3.5] - 2023-05-03

-   No changelog

## [2.3.4] - 2023-04-26

-   No changelog

## [2.3.3] - 2023-04-17

-   No changelog

## [2.3.2] - 2023-03-22

-   No changelog

## [2.3.1] - 2023-03-08

-   No changelog

## [2.3.0] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [2.2.0] - 2023-02-06

### Added

-   introduce dependency injection

### Changed

-   Change storage provider from S3 to Azure blob storage ([input-gateway#2]([https://gitlab.com/operator-ict/golemio/code/vp/input-gateway/-/issues/2]))

## [2.1.0] - 2023-01-23

### Changed

-   Docker image optimization
-   Migrate to npm

## [2.0.31] - 2023-01-10

-   No changelog

## [2.0.30] - 2023-01-04

### Changed

-   Update golemio/core to version with dependency injection

## [2.0.29] - 2022-11-29

-   No changelog

## [2.0.28] - 2022-11-14

### Added

-   Added warning for migration to Swagger UI

### Changed

-   Update TypeScript to v4.7.2

### Removed

-   Removed PID documentation and replaced by link to Swagger UI

## [2.0.27] - 2022-09-21

### Changed

-   Update Node.js to 16.17.0 ([code/general#418](https://gitlab.com/operator-ict/golemio/code/general/-/issues/418)

### Removed

-   Unused dependencies ([modules/general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))

