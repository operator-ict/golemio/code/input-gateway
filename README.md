[![pipeline status](https://gitlab.com/operator-ict/golemio/code/input-gateway/badges/master/pipeline.svg)](https://gitlab.com/operator-ict/golemio/code/input-gateway/commits/master)
[![coverage report](https://gitlab.com/operator-ict/golemio/code/input-gateway/badges/master/coverage.svg)](https://gitlab.com/operator-ict/golemio/code/input-gateway/commits/master)

# Golemio Data Platform Input Gateway

The Input Gateway serves as a pivotal component within the Golemio data platform system, providing a REST API for receiving (PUSH) data to be stored in the Golemio data platform.

This gateway employs an Express server for communication, and it interfaces with data processing through RabbitMQ.

For comprehensive documentation, please visit: [Golemio Input Gateway Documentation](https://operator-ict.gitlab.io/golemio/code/input-gateway/)

Refer to the API documentation at:

-   [Main (production) Input API Documentation](https://api.golemio.cz/input-gateway/docs/openapi/)
-   [Test (development) Input API Documentation](https://rabin.golemio.cz/input-gateway/docs/openapi/)

Developed by http://operatorict.cz

## Docker instalation

### Prerequisites

-   Docker Engine
-   RabbitMQ
-   AWS S3 (optional)
-   Golemio Schema Definitions

### Instalation

1. Build docker image by `docker build -t input-gateway .`
2. Setup ENV variables by `.env` file or add `-e VAR=VALUE` to docker run
3. Run container by

```
docker run --rm \
    -p 3005:3005 \ # expose port 3005
    -e PORT=3005 \
    -e RABBIT_CONN="amqp://rabbit:password@rabbit-service" \ # connection string for rabbitmq
    -e RABBIT_EXCHANGE_NAME="dataplatform-tutorial" \ # Rabbitmq exchange server name
    input-gateway # docker image label (defined by step 1)
```

## Local instalation

### Prerequisites

-   node.js
-   RabbitMQ
-   npm
-   typescript
-   Golemio Schema Definitions

### Instalation

Install all prerequisites

Install all dependencies using command:

```
npm install
```

from the application's root directory.

### Build & Run

#### Production

To compile typescript code into js one-time (production build):

```
npm run build
```

To run the app:

```
npm run start
```

#### Dev/debug

Run via TypeScript (in this case it is not needed to build separately, application will watch for changes and restart on save):

```
npm run dev-start
```

or run with a debugger:

```
npm run dev-start-debug
```

Configuration is split to environment (.env file) options and other specific options (e.g. saveRawDataWhitelist).

The specific configuration files are in the `config/` directory. For more information see [config files](https://gitlab.com/operator-ict/golemio/code/modules/core/-/blob/development/docs/configuration_files.md#input-gateway)

Runing the application in any way will load all config variables from environment variables or the .env file. To run, set all environment variables from the `.env.template` file, or copy the `.env.template` file into new `.env` file in root directory and set variables there.

Project uses `dotenv` package: https://www.npmjs.com/package/dotenv

Application is now running locally on port 3005 or on port specified in the environment variable.

### Data Processing

Data which are received by input gateway are validated and sent to RabbitMQ exchange. Because input gateway has no permissions to create the queues binded to exchange, by default exchange receive the message and discard it.

If you want to store the messages in the queue, you must use `golemio integration engine` or manually create testing queue and bind it with exchange. For creating the testing queues you can use [RabbitMQ Management UI](https://www.rabbitmq.com/management.html) ([manual](https://www.cloudamqp.com/blog/2015-05-27-part3-rabbitmq-for-beginners_the-management-interface.html)).

## Tests

To run all test defined in /test directory simply run this command:

```
npm run test
```

from the application's root directory.

## Logging

Logging uses `pino` for standard logging with levels, `pino-http` for http access logs and `debug` (https://www.npmjs.com/package/debug) for debugging.

All logs with `silly` and `debug` level are printed as standard log (if appropriate log level is set) using `pino` as well as using `debug` module with `"data-platform:input-gateway"` settings.

You can set both `LOG_LEVEL` and `DEBUG` settings in ENV variables.

## Documentation

For generating documentation run `npm run generate-docs`. Typedoc source code documentation is located in `docs/typedoc`.

More documentation in `docs/`. If you want to add a new dataset or create new API routes, check out our existing [modules](https://gitlab.com/operator-ict/golemio/code/modules).

### API specification

The Input API specifications are located within each module's `openapi-input.yaml`. You can find an illustrative example in the [Parkings module](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/blob/development/docs/openapi-input.yaml).

To generate the API documentation, execute the command `npm run build-apidocs`. The documentation will be generated in the `docs/generated` directory.

## Contribution guidelines

Please read [CONTRIBUTING.md](CONTRIBUTING.md).

## Problems?

Contact benak@operatorict.cz or vycpalek@operatorict.cz
