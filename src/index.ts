// Load reflection lib
import "@golemio/core/dist/shared/_global";

// Load telemetry before all deps
import { initTraceProvider } from "@golemio/core/dist/monitoring";
initTraceProvider();

// Start the app
import App from "./App";
new App().start();
