ARG BASE_IMAGE=node:20.18.0-alpine

FROM $BASE_IMAGE AS build
WORKDIR /app

COPY .npmrc package.json package-lock.json tsconfig.json ./
COPY src src
COPY docs docs
RUN npm install --ignore-scripts --progress=false && \
    npm run build-apidocs && \
    npm run build-minimal

FROM $BASE_IMAGE
WORKDIR /app

RUN apk --no-cache add dumb-init
COPY --from=build /app/dist /app/dist
COPY --from=build /app/docs/generated /app/docs/generated

COPY .npmrc package.json package-lock.json  ./
RUN npm install --ignore-scripts --progress=false --omit=dev --cache .npm-cache && \
    rm -rf .npm-cache .npmrc package-lock.json

# Remove busybox links
RUN busybox --list-full | \
    grep -E "bin/ifconfig$|bin/ip$|bin/netstat$|bin/nc$|bin/poweroff$|bin/reboot$" | \
    sed 's/^/\//' | xargs rm -f

# Create a non-root user
RUN addgroup -S nonroot && \
    adduser -S nonroot -G nonroot -h /app -u 1001 -D && \
    chown -R nonroot /app

# Disable persistent history
RUN touch /app/.ash_history && \
    chmod a=-rwx /app/.ash_history && \
    chown root:root /app/.ash_history

USER nonroot

EXPOSE 3000
ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["node", "-r",  "dotenv/config", "dist/index.js"]
